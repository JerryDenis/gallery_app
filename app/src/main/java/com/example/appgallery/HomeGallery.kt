package com.example.appgallery

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.size
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.google.android.material.textfield.TextInputLayout
import java.util.ArrayList

class HomeGallery : AppCompatActivity(), View.OnClickListener {
    var listaFoto : ArrayList<String>? = null
    var textError: TextInputLayout? = null;

    var adapter : GaleriaAdapter? = null

    var pos_img : Int = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_gallery)
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Home Gallery"


        val btnCargar = findViewById<Button>(R.id.btnCargar)
        val btnEliminar = findViewById<Button>(R.id.btnEliminar)
        val edtUrl = findViewById<EditText>(R.id.edtUrl)
        val imgFoto = findViewById<ImageView>(R.id.imgFoto)
        textError = findViewById(R.id.inputUrl)
        val imgitmgaleria = findViewById<ImageView>(R.id.imgItemGaleria)

        val lyListaRV = findViewById<RecyclerView>(R.id.lyListaRV)

        listaFoto = arrayListOf()

        adapter = GaleriaAdapter(listaFoto!!, this)

        lyListaRV!!.adapter = adapter
        lyListaRV!!.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false)

        btnCargar.setOnClickListener {

            val url = edtUrl.text.toString()
            textError?.error = null
            if (TextUtils.isEmpty(url)) {
                textError?.error = "Ingrese Url"
            }else if (url.trim() == "") {
                textError?.error = "Ingrese Url"
            }else {

                lyListaRV.visibility = View.VISIBLE
                imgFoto.visibility = View.VISIBLE
                if (this.listaFoto!!.size == 6){
                    Toast.makeText(applicationContext,"La lista de imagenes esta completo",
                        Toast.LENGTH_LONG).show()
                }else{
                    imgFoto.load(url) {
                        crossfade(true)
                        placeholder(R.drawable.ic_launcher_background)
                        transformations(CircleCropTransformation())
                        listaFoto!!.add(url)
                        edtUrl.text.clear()
                        adapter!!.notifyDataSetChanged()
                    }

                }

            }

        }

        btnEliminar.setOnClickListener {


            if (lyListaRV.size == 0){
                Toast.makeText(
                    applicationContext,
                    "CARGA UNA IMAGEN URL",
                    Toast.LENGTH_LONG
                ).show()

            } else {

                if (pos_img >= 0 && this.listaFoto!!.size > 0 ) {
                    this.listaFoto!!.removeAt(pos_img)
                    adapter!!.notifyDataSetChanged()
                    lyListaRV!!.scrollToPosition(0)
                    val url = this.listaFoto!!.get(0)
                    imgFoto.load(url) {
                        crossfade(true)
                        placeholder(R.drawable.ic_launcher_background)
                        transformations(CircleCropTransformation())

                    }

                }
                else{
                    Toast.makeText(
                        applicationContext,
                        "Seleccione un elemento de la lista",
                        Toast.LENGTH_LONG
                    ).show()


                }
            }

        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.nav_menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.new_catalogo -> {
                catalogo()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
    fun catalogo(){
        val inte = Intent(this@HomeGallery, MosaicoGallery::class.java)
        inte.putStringArrayListExtra("lista_img",listaFoto)
        startActivity(inte)
    }

    override fun onClick(p0: View?) {
        if(p0!!.id == R.id.imgItemGaleria){

            val pos = p0!!.tag as Int
            pos_img = pos
            val imgFoto = findViewById<ImageView>(R.id.imgFoto)
            val url = this.listaFoto!!.get(pos)
            imgFoto.load(url) {
                crossfade(true)
                placeholder(R.drawable.ic_launcher_background)
                transformations(CircleCropTransformation())
            }

        }

    }
}