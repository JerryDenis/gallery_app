package com.example.appgallery

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.ly_dialog_custom.view.*

class MainActivity : AppCompatActivity(),View.OnClickListener {
    var textError: TextInputLayout? = null
    var passError: TextInputLayout? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textError = findViewById(R.id.inputCorreo)
        passError = findViewById(R.id.inputClave)

        //setSupportActionBar(findViewById(R.id.toolbar))
        var btnSesionStart: Button = findViewById(R.id.btnIniciar)
        btnSesionStart.setOnClickListener(this)
    }
    override fun onClick(v: View?) {
        val edtemail = findViewById<EditText>(R.id.edtCorreo)
        val edtpass = findViewById<EditText>(R.id.edtClave)

        if (v?.id == R.id.btnIniciar) {
            val dialog = this.dialog("Validando Usuario")
            val user: String = edtemail.text.toString()
            val clave: String = edtpass.text.toString()
            textError?.error = null
            passError?.error = null
            if (TextUtils.isEmpty(user)) {
                textError?.error = "Ingrese Correo"
            } else if (TextUtils.isEmpty(clave)) {
                passError?.error = "Ingrese Clave"
            } else if (user.trim() == "") {
                textError?.error = "Ingrese Clave"
            } else if (clave.trim() == "") {
                passError?.error = "Ingrese Clave"
            } else {
                print("correo y clave no vacios")

                dialog.show()
                if (user == "jerry" && clave == "123") {

                    startActivity(Intent(this, HomeGallery::class.java))
                    startActivity(intent)
                    dialog.dismiss()
                    finish()
                } else {
                    //error

                    Toast.makeText(
                        applicationContext,
                        "Login no exitoso, error en las credenciales, verificar por favor",
                        Toast.LENGTH_LONG
                    ).show()


                }


            }

        }
    }


    fun dialog(msg: String): MaterialDialog {
        val dialog: MaterialDialog = MaterialDialog(this)
            .customView(R.layout.ly_dialog_custom, scrollable = true)
        dialog.cancelOnTouchOutside(false)
        val customView = dialog.getCustomView()
        customView.messa.setText(msg)
        return dialog
    }
}