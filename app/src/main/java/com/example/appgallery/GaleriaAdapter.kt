package com.example.appgallery

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation

class GaleriaAdapter(var lista : ArrayList<String>, var listener: View.OnClickListener) : RecyclerView.Adapter<GaleriaAdapter.GaleriaViewHolder>() {

    class GaleriaViewHolder(val row: View) : RecyclerView.ViewHolder(row) {

        val imgItemGaleria : ImageView = row.findViewById(R.id.imgItemGaleria)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GaleriaViewHolder {
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.ly_item_galeria,parent,false)
        return GaleriaViewHolder(layout)

    }

    override fun onBindViewHolder(holder: GaleriaViewHolder, position: Int) {
        val myUrl = lista.get(position)

        holder.imgItemGaleria.load(myUrl) {
            crossfade(true)
            placeholder(R.drawable.ic_launcher_background)
            //transformations(CircleCropTransformation())
        }

        holder.imgItemGaleria.tag = position
        holder.imgItemGaleria.setOnClickListener(this.listener)


    }

    override fun getItemCount(): Int {
        return lista.size
    }


}