package com.example.appgallery


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class MosaicoGallery : AppCompatActivity() {
    var listaGrid : ArrayList<String>? = null
    var adaptergrid : MosaicoAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mosaico_gallery)
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = "Mosaico Gallery"
        actionbar.setDisplayHomeAsUpEnabled(true)
        val lyListaGR = findViewById<RecyclerView>(R.id.lyListaGR)


        listaGrid = arrayListOf()

        val extras = this.intent.extras

        val listaAux = extras!!.getStringArrayList("lista_img")

        listaGrid = listaAux

        adaptergrid = MosaicoAdapter(listaGrid!!)

        lyListaGR!!.adapter = adaptergrid
        lyListaGR!!.layoutManager = GridLayoutManager(this,2, RecyclerView.VERTICAL,false)

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}