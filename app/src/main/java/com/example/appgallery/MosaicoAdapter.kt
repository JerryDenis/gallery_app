package com.example.appgallery


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load


class MosaicoAdapter(var listagrid : ArrayList<String>) : RecyclerView.Adapter<MosaicoAdapter.MosaicoViewHolder>() {

    class MosaicoViewHolder(val row: View) : RecyclerView.ViewHolder(row) {

        val imgItemGrid : ImageView = row.findViewById(R.id.imgItemGrid)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MosaicoViewHolder{
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.ly_item_grid,parent,false)
        return MosaicoViewHolder(layout)

    }

    override fun onBindViewHolder(holder: MosaicoViewHolder, position: Int) {
        val myUrl = listagrid.get(position)

        holder.imgItemGrid.load(myUrl) {
            crossfade(true)
            placeholder(R.drawable.ic_launcher_background)
            //transformations(CircleCropTransformation())
        }

        holder.imgItemGrid.tag = myUrl

    }

    override fun getItemCount(): Int {
        return listagrid.size
    }


}